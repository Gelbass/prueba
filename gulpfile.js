const {src, dest, series, parallel, gulp, task} = require("gulp");
const sass = require('gulp-dart-scss');
const pleeease = require('gulp-pleeease');
const sassdoc = require('sassdoc');
const rename = require('gulp-rename');
const processhtml = require('gulp-processhtml');
const inject = require('gulp-inject');

function borrar(cb) {
    del("./dist/*");
    cb();
}
function build_css() {
    return src("scss/estilos.scss")
    .pipe(sass())
    .pipe(pleeease())
    .pipe(
      rename({
        basename: "styles",
        suffix: ".min",
        extname: ".css"
      }))    
    .pipe(dest('dist/css/'));
}
function des_build_css() {
    return src("scss/estilos.scss")
    .pipe(sass())
    .pipe(pleeease())
    .pipe(
      rename({
        basename: "styles",
        suffix: ".min",
        extname: ".css"
      }))    
    .pipe(dest('css/'));
}

function build_docs() {
    var doc_options = {
        dest : "./dist/docs",
        verbose: true
    }
     return src("./scss/*.scss")
    .pipe(sassdoc(doc_options));
 }

function mover_js(){
    return src('./js/*')
    .pipe(dest('./dist/js'));
}

function mover_img() {
    return src('./img/*')
    .pipe(dest('./dist/img'));
}
function mover_slick() {
    return src('./slick/*')
    .pipe(dest('./dist/slick'));
}

function mover_html() {
    return src("./index.html")
    .pipe(processhtml())
    .pipe(dest('./dist'));
}
function insertar_js_css(){    
    gulp.task('index', function () {
    var target = gulp.src('./index.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src(['./dist/js/*.js', './dist/css/*.css'], {read: false});
    
    return target.pipe(inject(sources))
        .pipe(gulp.dest('./dist'));
});
} 

exports.build_css = build_css;
exports.des_build_css = des_build_css;
exports.build_docs = build_docs;
exports.mover_js = mover_js;
exports.mover_img = mover_img;
exports.mover_slick = mover_slick
exports.mover_html = mover_html;
exports.insertar_js_css = insertar_js_css;
exports.default= series(parallel(build_css,build_docs),
                        parallel(mover_js, mover_img,mover_slick, mover_html));

