//Funcion Slick
$(function(){
  $('.single-item').slick();
})
//Scrip para limpiar Select
function limpiarSelect(id){
	var selectObj = document.getElementById(id);
	var selectParentNode = selectObj.parentNode;
	var newSelectObj = selectObj.cloneNode(false);
	selectParentNode.replaceChild (newSelectObj, selectObj);
	return newSelectObj;
}