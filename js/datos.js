var dptosLocs = {
	"Artigas": ["Artigas", "Bella Unión"],
	"Canelones": ["Canelones", "Santa Lucía"],
	"Montevideo": ["Montevideo"],
	"Salto": ["Salto", "Daymán", "Arapey"]
};

function cargarDepartamento() {
	var selDepartamento = document.getElementById("departamento");
	for (var depto in dptosLocs) {
		var opcionDepto = document.createElement("option");
		opcionDepto.value = depto;
		opcionDepto.text = depto;
		selDepartamento.appendChild(opcionDepto);
	}


}
 
function cargarLocalidad() {
	limpiarSelect("localidad");
	var selecion = document.getElementById("departamento").value;
	var selLocalidad = document.getElementById("localidad");
	
	for (var depto in dptosLocs) {
		for (var localidad in dptosLocs[depto]) {
			if (depto == selecion) {
				var opcionLocal = document.createElement("option");
				console.log(dptosLocs[depto][localidad]);
				opcionLocal.value = dptosLocs[depto][localidad];
				opcionLocal.text = dptosLocs[depto][localidad];
				selLocalidad.appendChild(opcionLocal)
			}
		}
	}
}
