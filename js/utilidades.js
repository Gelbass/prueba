//Utilides de validacion
function validarFormulario(){
    //Validacion componente Nombre
    var nombre = document.getElementById("nombre")
    if(nombre.value == "" || nombre.value == "Nombre"){
        document.getElementById("mensajeError").innerHTML = "Favor ingrese Nombre";
        nombre.style.cssText= "border: 3px solid #a80303";
        nombre.focus();
        return false;
    };
    //Validacion componente Apellido
    var apellido = document.getElementById("apellido")
    if(apellido.value == "" || apellido.value == "Apellido"){
        document.getElementById("mensajeError").innerHTML = "Favor ingrese Apellido";
        apellido.focus();
        apellido.style.cssText= "border: 3px solid #a80303";
        return false;
    };
    
    //Validar seleccion Departamento
    var departamento = document.getElementById("departamento")
    if(departamento.value == "Departamento"){
        document.getElementById("mensajeError").innerHTML = "Favor seleccione el Departamento";
        departamento.style.cssText= "border: 3px solid #a80303";
        return false;
    }//Validar seleccion Localidad
    var localidad = document.getElementById("localidad")
    if(localidad.value == "Localidad" || localidad.value == ""){
        document.getElementById("mensajeError").innerHTML = "Favor seleccione la Localidad";
        localidad.style.cssText= "border: 3px solid #a80303";
        return false;
    };
    //Validacion de componente Cedula
    var ci = document.getElementById("ci");
        if(ci.value == "" || ci.value == "C.I"){
            document.getElementById("mensajeError").innerHTML = "Favor ingrese C�dula de Identidad valido";
            ci.style.cssText= "border: 3px solid #a80303";
            return false;
        };
        console.log("salio del al validar");
    //Validacion de terminos
    var aceptar = document.getElementById("aceptCondiciones").checked;
    if(!aceptar.checked){
        document.getElementById("mensajeError").innerHTML = "Favor aceptar las condiciones";
        
        return false
    };
    //Formulario es valido
    document.getElementById("mensajeError").innerHTML = "";
    alert("Formulario valido, enviado datos al servidor");
    console.log("valido");
    return true;
}

